package com.example.user.parkinglot;
import android.provider.BaseColumns;

/**
 * Created by user on 03.02.2018.
 */

public final class MyDatabaseContract {
    public MyDatabaseContract() {}
    public static final String DB_NAME = "MyDatabase.db";
    /* Inner class that defines the table contents */

    public static abstract class TableLots implements BaseColumns {
        public static final String TABLE_NAME = "lots";
        public static final String COLUMN_NAME_LOT_ID = "_id";
        public static final String COLUMN_NAME_LOT_NAME = "lotname";
        public static final String COLUMN_NAME_IS_FAVORITEE = "favor";
        public static final String COLUMN_NAME_ESTIMATE = "est";
        public static final String COLUMN_NAME_LATITUDE = "lat";
        public static final String COLUMN_NAME_LONGITUDE = "long";
    }
}



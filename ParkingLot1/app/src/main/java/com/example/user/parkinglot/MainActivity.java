package com.example.user.parkinglot;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity  {
    public final static String EXTRA_MESSAGE = "com.mycompany.myfirstapp.MESSAGE";
    public final static String EXTRA_MESSAGE1 = "com.mycompany.myfirstapp.MESSAGE1";
    int fav;
   // RatingBar rating1;
    SharedPreferences.Editor myEditor;
    TextView textView;
    SharedPreferences setting;
    MyDatabaseHelper mDbHelper;
    SQLiteDatabase dbRead;
    SQLiteDatabase dbWrite;
    String lon;
    String lat,lati;
    EditText etID;
    EditText latID;
    EditText lonID;
    CheckBox checkBoxFavor;
    LinearLayout out,out2,out1;
    Button save1Bt,delID;
    Button editBt;
    int esti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDbHelper = new MyDatabaseHelper(this);
        dbWrite = mDbHelper.getWritableDatabase();
        dbRead = mDbHelper.getReadableDatabase();
        textView = findViewById(R.id.textView);
        latID=findViewById(R.id.LatEdit);
        lonID=findViewById(R.id.LonEdit);
        etID=findViewById(R.id.etID);
        out=findViewById(R.id.out);
        out1=findViewById(R.id.out1);
        out2=findViewById(R.id.out2);
        delID=findViewById(R.id.delID);
        save1Bt=findViewById(R.id.save1Bt);
        editBt=findViewById(R.id.editBt);
        TextView textView = (TextView) findViewById(R.id.textView);
        checkBoxFavor=findViewById(R.id.checkBoxFavor);
        setting=getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        myEditor=setting.edit();

       // rating1=findViewById(R.id.rating1);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

    }
    public void goMap(View view)
    {
        String inp=etID.getText().toString();
        boolean b=inputsAreCorrect(inp);

        if(b){
            readData2();
        textView.setText("");
        Intent intent = new Intent(this, StoryActivity.class);
        String message = latID.getText().toString();
       /*
      intent.putExtra(EXTRA_MESSAGE, message);
      myEditor.putString(EXTRA_MESSAGE1,message);
      myEditor.commit();
      startActivity(intent);
      */

            reset1();
            intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);

            intent.setData(Uri.parse("geo:"+message));
            startActivity(intent);
    }}
    private boolean inputsAreCorrect(String name) {
        if (name.trim().isEmpty()) {

            return inputAreCorrect(   name) ;
        }
        return true;
    }
    private boolean inputAreCorrect(String name) {

            etID.setError(getString(R.string.enterNumber));
            etID.requestFocus();
            return false;

    }
    public void editLot(View view)
    {
        boolean b=inputsAreCorrect(etID.getText().toString());

        if(b)
        readData2();


    }
    public void deletLot(View view)
    {
        String selection = MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID + " LIKE ?";
// Specify arguments in placeholder order
        String inp=etID.getText().toString();
        boolean b=inputsAreCorrect(inp);

        if(b){
        int id=Integer.valueOf(inp);
        String[] selectionArgs = { String.valueOf(id) };

        dbRead.delete(MyDatabaseContract.TableLots.TABLE_NAME, selection, selectionArgs);
        readData();
        reset1();
    }}
    public void readData(View view)
    {
        readData(   );}

    public boolean readData2() {

        String[] projection ={  MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID,   MyDatabaseContract.TableLots.COLUMN_NAME_LOT_NAME,
                MyDatabaseContract.TableLots.COLUMN_NAME_IS_FAVORITEE,     MyDatabaseContract.TableLots.COLUMN_NAME_ESTIMATE, MyDatabaseContract.TableLots.COLUMN_NAME_LATITUDE };
        String sortOrder= MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID + " DESC";
       // Which row to update, based on the ID
        String selection = MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID + " LIKE ?";
        int id=Integer.valueOf(etID.getText().toString());
        String[] selectionArgs = { String.valueOf(id) };
       try {
           Cursor myCursor = dbRead.query(
                   MyDatabaseContract.TableLots.TABLE_NAME,
                   projection,
                   selection,//where (list of database fields to filter from = selection)
                   selectionArgs,//where (list of values\conditions to filter = selectionArgs)
                   null,//where of grouping (list of database fields to filter from
                   null,//where  of grouping (list of values\conditions to filter
                   sortOrder
           );
           textView.setText("");
           out.setVisibility(View.VISIBLE);
          // out4.setVisibility(View.VISIBLE);
           out1.setVisibility(View.GONE);
           out2.setVisibility(View.GONE);
           myCursor.moveToFirst();
           lon = myCursor.getString(1);
           fav = myCursor.getInt(2);
           esti = myCursor.getInt(3);
           lat = myCursor.getString(4);
           lonID.setText(lon);
           latID.setText(lat);
        //   rating1.setRating((float)esti);
           checkBoxFavor.setChecked(fav == 1);

       }
       catch(Exception ex) {
           inputAreCorrect("");
           reset1();
           return false;
       }
       return true;
    }

    public void readData(){
        textView.setText("");
        Cursor cursor = dbWrite.query(MyDatabaseContract.TableLots.TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            String name = cursor.getString(1);
            int favor = cursor.getInt(2);
            int rate=cursor.getInt(3);
              lati=cursor.getString(4);

            textView.append(cursor.getString(0)+": "+getString(R.string.fname_label)+" "+ name +", "+ getString(R.string.rate)+": " + rate + favor(favor)+"\n"+
                    lati+ "\n");
        }
        cursor.close();

    }
    public void goSave(View v){
        ContentValues values = new ContentValues();
        String late=latID.getText().toString();
        String lote=lonID.getText().toString();
        if(checkBoxFavor.isChecked())
        fav=1;  else fav=0;
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_IS_FAVORITEE, fav);
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_LATITUDE,late);
        values.put(MyDatabaseContract.TableLots.COLUMN_NAME_LOT_NAME,lote);
      //  values.put(MyDatabaseContract.TableLots.COLUMN_NAME_ESTIMATE, (int)rating1.getRating());
        String selection = MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID + " LIKE ?";
        int id=Integer.valueOf(etID.getText().toString());
        String[] selectionArgs = { String.valueOf(id) };

        int counted = dbRead.update(
                MyDatabaseContract.TableLots.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        reset1();
    }
    public void deletData(View v){
         dbRead.delete(MyDatabaseContract.TableLots.TABLE_NAME, null, null);
        // dbWrite.execSQL("DELETE FROM "+MyDatabaseContract.TableLots.TABLE_NAME);
        readData( );
    }
    private String favor(int s){
        String str=", ";
        if(s==0){ str+=getString(R.string.no);
            str+=" ";}
        return str+=getString(R.string.food);

    }
    public void reset1(){
        out.setVisibility(View.GONE);
        out1.setVisibility(View.VISIBLE);
        out2.setVisibility(View.VISIBLE);
        etID.setText("");
    }
    public void favorGet(View view){
        textView.setText("");
        Cursor cursor = dbWrite.query(MyDatabaseContract.TableLots.TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int favor = cursor.getInt(2);
            if(favor==1){
            String name = cursor.getString(1);

            int rate=cursor.getInt(3);
                     lati=cursor.getString(4);

            textView.append(cursor.getString(0)+": "+getString(R.string.fname_label)+" "+ name +", "+ getString(R.string.rate)+": " + rate + "\n"+
                    lati +"\n");
        }}
        cursor.close();
        reset1();
    }
    public void getBest(View view){
        textView.setText("");
        Cursor cursor = dbWrite.query(MyDatabaseContract.TableLots.TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            int favor = cursor.getInt(3);
            if(favor==5){
                String name = cursor.getString(1);

                int rate=cursor.getInt(3);
                lati=cursor.getString(4);
                textView.append(cursor.getString(0)+": "+getString(R.string.fname_label)+" "+ name +", "+ getString(R.string.rate)+": " + rate + favor(favor)+"\n"+
                        lati+ "\n");
            }}
        cursor.close();
        reset1();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent nextActivity;
        reset1();
        if (id == R.id.action_home) {
            return true;
        }
        else if(id == R.id.action_near){
            nextActivity=new Intent(this,NearActivity.class);
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,SettingActivity.class);
        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }
}

package com.example.user.parkinglot;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class NearActivity extends AppCompatActivity implements LocationListener {
    WebView myWeb;
    TextView myTextView;
    LocationManager myLocationManager;
    Location firstLocation;
    SharedPreferences settings;
    String locationStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        settings = getSharedPreferences(SettingActivity.FIRST_SHARED_FILE, 0);
        myWeb = findViewById(R.id.webViewLocation);
        myTextView = findViewById(R.id.textViewLocation);
        myLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        myLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3 * 1000, 10, this);
        getLoc();
        myTextView.setText(locationStr);

        myWeb.setWebViewClient(new WebViewClient());
        WebSettings settings = myWeb.getSettings();
        settings.setJavaScriptEnabled(true);
        //https://www.google.com/maps/place/43.12345,-76.12345
        // String strUrl="https://www.google.com/maps/place/"+firstLocation.getLatitude()+","+firstLocation.getLongitude();
        // display lots near the location

        String strUrl = "https://www.google.co.il/maps/search/lots/@" + firstLocation.getLatitude() + "," + firstLocation.getLongitude() + ",13z";
        myWeb.loadUrl(strUrl);
    }

    private void getLoc() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            firstLocation = myLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
             locationStr = firstLocation.getLongitude() + ", " + firstLocation.getLatitude();
        }
        catch (Exception ex) {
            firstLocation=new Location("");
            String lat=settings.getString(SaveActivity.FIRST_LATITUDE,"32");
            String  lon=settings.getString(SaveActivity.FIRST_LONGITUDE,"33");
            firstLocation.setLongitude(Double.parseDouble(lon));
            firstLocation.setLatitude(Double.parseDouble(lat));
            locationStr = firstLocation.getLongitude() + ", " + firstLocation.getLatitude();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent nextActivity;

        if (id == R.id.action_home) {
            nextActivity=new Intent(this,MainActivity.class);
        }

        else if(id == R.id.action_near){
            return true;
        }
        else if(id == R.id.action_save){
            nextActivity=new Intent(this,SaveActivity.class);
        }
        else if(id==R.id.action_story){
            nextActivity=new Intent(this,StoryActivity.class);
        }
        else if (id == R.id.action_team) {
            nextActivity=new Intent(this,TeamActivity.class);
        }

        else nextActivity=new Intent(this,SettingActivity.class);
        startActivity(nextActivity);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}

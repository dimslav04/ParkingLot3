package com.example.user.parkinglot;

/**
 * Created by user on 03.02.2018.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = MyDatabaseContract.DB_NAME;
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " DOUBLE";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_LOTS =
            "CREATE TABLE " + MyDatabaseContract.TableLots.TABLE_NAME  + " (" +
                    MyDatabaseContract.TableLots.COLUMN_NAME_LOT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MyDatabaseContract.TableLots.COLUMN_NAME_LOT_NAME + TEXT_TYPE + COMMA_SEP  +
                    MyDatabaseContract.TableLots.COLUMN_NAME_IS_FAVORITEE + INTEGER_TYPE + COMMA_SEP +
                    MyDatabaseContract.TableLots.COLUMN_NAME_ESTIMATE + INTEGER_TYPE  +
                    MyDatabaseContract.TableLots.COLUMN_NAME_LATITUDE + TEXT_TYPE +  " )";
    private static final String SQL_DELETE_LOTS =
            "DROP TABLE IF EXISTS " + MyDatabaseContract .TableLots.TABLE_NAME;
    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_LOTS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       // db.execSQL(SQL_DELETE_LOTS);
       // onCreate(db);
        if(newVersion>oldVersion && newVersion==2 ){
            String str=             "ALTER TABLE "+ MyDatabaseContract.TableLots.TABLE_NAME + " ADD COLUMN " + MyDatabaseContract.TableLots.COLUMN_NAME_LATITUDE+ TEXT_TYPE;

            db.execSQL(str);
    }
  else if(newVersion>oldVersion && newVersion==3 ){
            String str=             "ALTER TABLE "+ MyDatabaseContract.TableLots.TABLE_NAME + " ADD COLUMN " + MyDatabaseContract.TableLots.COLUMN_NAME_LATITUDE+ TEXT_TYPE;

            db.execSQL(str);
        }}

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);        }

}